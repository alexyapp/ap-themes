<?php

return [

    'colours' => [
        'font-family-url' => 'https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,500&display=swap',
        'font-family' => '"Montserrat", sans-serif',
        'primary' => '#5386e4',
        'secondary' => '#333240',
        'tertiary' => '#4c4b63',
        'text_colour' => '#212121',
        'cta-background' => '#ffa500',
        'cta-text' => '#ffffff',
        // new nav
        'nav-background-colour' => '#333240',
        'nav-text-colour' => '#ffffff',
        // new footer
        'footer-background-color' => '#4c4b63',
        'footer-text-colour' => '#ffffff',
        'footer-link-colour' => '#ffffff'


    ],

    'backgrounds' => [
        'home.hero' => [
            'filepath' => 'backgrounds/home-hero.jpg',
            'linear_gradient_start' => '000000',
            'linear_gradient_end' => '000000',
            'linear_gradient_opacity_start' => '0.3',
            'linear_gradient_opacity_end' => '0.3',
        ],
        'home.about' => [
            'filepath' => 'backgrounds/home-about.jpg',
            'linear_gradient_start' => null,
            'linear_gradient_end' => null,
            'linear_gradient_opacity_start' => null,
            'linear_gradient_opacity_end' => null,
        ],
        'valuation.hero' => [
            'filepath' => 'backgrounds/valuation.jpg',
            'linear_gradient_start' => '000000',
            'linear_gradient_end' => '000000',
            'linear_gradient_opacity_start' => '0.3',
            'linear_gradient_opacity_end' => '0.3',
        ],
        'contact.hero' => [
            'filepath' => 'backgrounds/contact.jpg',
            'linear_gradient_start' => '000000',
            'linear_gradient_end' => '000000',
            'linear_gradient_opacity_start' => '0.85',
            'linear_gradient_opacity_end' => '0.85',
        ],
        'about.hero' => [
            'filepath' => 'backgrounds/about.jpg',
            'linear_gradient_start' => '000000',
            'linear_gradient_end' => '000000',
            'linear_gradient_opacity_start' => '0.5',
            'linear_gradient_opacity_end' => '0.5',
        ],
        'area-guide.hero' => [
            'filepath' => 'backgrounds/area-guide.jpg',
            'linear_gradient_start' => '000000',
            'linear_gradient_end' => '000000',
            'linear_gradient_opacity_start' => '0.5',
            'linear_gradient_opacity_end' => '0.5',
        ],
    ],

];
