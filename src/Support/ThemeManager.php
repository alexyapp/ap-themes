<?php

namespace Propertywebmasters\AgentPlusThemes\Support;

use App\Models\TenantMedia;
use App\Repositories\TenantMediaRepository;
use Illuminate\Support\Facades\Storage;

/**
 * Class ThemeManager
 * @package App\Support
 */
class ThemeManager
{

    public $tenant;

    /** @var string */
    public $theme;

    /** @var array */
    public $config;

    /**
     * ThemeManager constructor.
     * @param $tenant
     */
    public function __construct($tenant)
    {
        $this->tenant = $tenant;
        $this->theme = $tenant->theme;
        $this->setConfiguration();
    }

    /**
     * @param $key
     * @param  null  $default
     * @return mixed|null
     */
    public function background($key, $default = null)
    {
        return $this->config['backgrounds'][$key] ?? $default;
    }

    /**
     * @param $key
     * @param  null  $default
     * @return mixed|null
     */
    public function colour($key, $default = null)
    {
        return $this->config['colours'][$key] ?? $default;
    }

    /**
     * Applies the config array in config.php to this object
     */
    private function setConfiguration(): void
    {
        $this->config = include base_path('vendor/propertywebmasters/agentplus-themes/src/Themes/'.$this->theme.'/config.php');
    }

    /**
     * @param $media
     * @return string|null
     */
    public static function backgroundImagePath($media)
    {
        if ($media === null) {
            return null;
        }

        return self::imagePath($media->filepath);
    }

    /**
     * @param  string  $viewPath
     * @param  null  $tenant
     * @return string|string
     * @throws \Exception
     */
    private static function viewPath(string $viewPath, $tenant = null)
    {
        if ($tenant === null) {
            return $viewPath;
        }

        if (view()->exists('themes.'.$tenant->theme.'.'.$viewPath)) {
            return 'themes.'.$tenant->theme.'.'.$viewPath;
        }

        return $viewPath;
    }

    /**
     * Returns s3 storage path or nothing prefix when dealing with relative urls
     *
     * @param  string  $path
     * @return string
     */
    private static function imagePath($filepath): string
    {
        return Storage::disk('s3')->url($filepath);
    }

    /**
     * @param string $identifier
     * @param $tenant
     * @return string
     */
    public static function backgroundCSS($identifier = '', $tenant): string
    {
        $foundClass = false;
        $repositories = [
            '\App\Repositories\TenantMediaRepository',
            '\App\Repositories\Tenant\TenantMediaRepository',
        ];

        // load different namespaces dependong on system loading this package in
        foreach ($repositories as $namespace) {
            if (class_exists($namespace)) {
                $foundClass = $namespace;
                break;
            }
        }

        if ($foundClass === false) {
            throw new Exception('Unable to find repository');
        }

        $repository = new $foundClass;

        $media = $repository->getWithIdentifier($identifier, $tenant);
        $themeConfig = (new self($tenant))->config;

        $imagePath = self::backgroundImagePath($media);

        if ($imagePath === null) {
            $imagePath = '/img/themes/'.$tenant->theme.'/assets/'.$themeConfig['backgrounds'][$identifier]['filepath'];
        }

        $gradient = 'linear-gradient(rgba([FROM_A],[FROM_B],[FROM_C],[FROM_OPACITY]), rgba([TO_A],[TO_B],[TO_C],[TO_OPACITY])), ';
        $bgImage = 'url([FILEPATH]);';
        $str = 'background: linear-gradient(rgba(0,0,0,.50), rgba(0,0,0,.50)), url([FILEPATH]);';

        $shouldReplaceFrom = isset($media->linear_gradient_start) && $media->linear_gradient_start !== null;
        $shouldReplaceFromOpacity =  isset($media->linear_gradient_opacity_start) && $media->linear_gradient_opacity_start !== null;
        $shouldReplaceTo = isset($media->linear_gradient_end) && $media->linear_gradient_end !== null;
        $shouldReplaceToOpacity =  isset($media->linear_gradient_opacity_end) && $media->linear_gradient_opacity_end !== null;

        $fromAValue = isset($media->linear_gradient_start) && $media->linear_gradient_start !== null
            ? hexdec(substr($media->linear_gradient_start, 0, 2))
            : hexdec(substr($themeConfig['backgrounds'][$identifier]['linear_gradient_start'], 0, 2));

        $fromBValue = isset($media->linear_gradient_start) && $media->linear_gradient_start !== null
            ? hexdec(substr($media->linear_gradient_start, 0, 2))
            : hexdec(substr($themeConfig['backgrounds'][$identifier]['linear_gradient_start'], 2, 2));

        $fromCValue = isset($media->linear_gradient_start) && $media->linear_gradient_start !== null
            ? hexdec(substr($media->linear_gradient_start, 0, 2))
            : hexdec(substr($themeConfig['backgrounds'][$identifier]['linear_gradient_start'], 4, 2));

        $toAValue = isset($media->linear_gradient_end) && $media->linear_gradient_end !== null
            ? hexdec(substr($media->linear_gradient_end, 0, 2))
            : hexdec(substr($themeConfig['backgrounds'][$identifier]['linear_gradient_end'], 0, 2));

        $toBValue = isset($media->linear_gradient_end) && $media->linear_gradient_end !== null
            ? hexdec(substr($media->linear_gradient_end, 0, 2))
            : hexdec(substr($themeConfig['backgrounds'][$identifier]['linear_gradient_end'], 2, 2));

        $toCValue = isset($media->linear_gradient_end) && $media->linear_gradient_end !== null
            ? hexdec(substr($media->linear_gradient_end, 0, 2))
            : hexdec(substr($themeConfig['backgrounds'][$identifier]['linear_gradient_end'], 4, 2));

        $fromOpacity = isset($media->linear_gradient_opacity_start) && $media->linear_gradient_opacity_start !== null
            ? $media->linear_gradient_opacity_start
            : $themeConfig['backgrounds'][$identifier]['linear_gradient_opacity_start'];

        $toOpacity = isset($media->linear_gradient_opacity_end) && $media->linear_gradient_opacity_end !== null
            ? $media->linear_gradient_opacity_end
            : $themeConfig['backgrounds'][$identifier]['linear_gradient_opacity_end'];

        if ($fromOpacity === null) {
            $fromOpacity = 0;
        }

        if ($toOpacity === null) {
            $toOpacity = 0;
        }

        $replacements = [
            '[FROM_A]' => $fromAValue,
            '[FROM_B]' => $fromBValue,
            '[FROM_C]' => $fromCValue,
            '[FROM_OPACITY]' => $fromOpacity,
            '[TO_A]' => $toAValue,
            '[TO_B]' => $toBValue,
            '[TO_C]' => $toCValue,
            '[TO_OPACITY]' => $toOpacity,
            '[FILEPATH]' => $imagePath,
        ];

        $css = $replacements['[FROM_A]'] !== null ? 'background: '.$gradient.$bgImage : 'background: '.$bgImage;
        $css = str_replace(array_keys($replacements), array_values($replacements), $css);

        return $css;
    }

}
