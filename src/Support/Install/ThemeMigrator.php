<?php

namespace Propertywebmasters\AgentPlusThemes\Support\Install;

/**
 * Class ThemeMigrator
 * @package Propertywebmasters\AgentPlusThemes\Support\Install
 */
class ThemeMigrator
{

    public static function migrate()
    {
        // @todo If you are unfortunate enough to run windows you will need a windows compatible of the cp -r code
        // final destinations
        // View files: resources/views/themes/default
        // Assets: public/img/themes/default

        $themes = [];

        $vendorPath = getcwd().'/vendor/propertywebmasters/agentplus-themes/src/Themes';
        $tmpThemes = scandir($vendorPath);

        foreach ($tmpThemes as $tmpTheme) {
            if (is_dir($vendorPath.'/'.$tmpTheme) && $tmpTheme !== '.' && $tmpTheme !== '..') {
                $themes[] = $tmpTheme;
            }
        }

        foreach ($themes as $theme) {

            $copyThemeFrom = $vendorPath.'/'.$theme.'/views';
            $copyThemeTo = resource_path('views/themes/'.$theme);

            $copyAssetsFrom = $vendorPath.'/'.$theme.'/assets';
            $copyAssetsTo = public_path('img/themes/'.$theme);

            if (!is_dir($copyThemeTo)) {
                mkdir($copyThemeTo, 0755, true);
            }

            if (!is_dir($copyAssetsTo)) {
                mkdir($copyAssetsTo, 0755, true);
            }

            $copyTheme = 'cp -r '.$copyThemeFrom.'    '.$copyThemeTo;
            $copyAssets = 'cp -r '.$copyAssetsFrom.'     '.$copyAssetsTo;

            // copy the view files to local filesystem
            exec($copyTheme);
            print "Copied themes from [{$copyThemeFrom}] to [{$copyThemeTo}]\n";

            // copy assets to the local filesystem
            exec($copyAssets);
            print "Copied assets from [{$copyAssetsFrom}] to [{$copyAssetsTo}]\n";
        }
    }

}
